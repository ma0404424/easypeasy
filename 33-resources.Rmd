# Resources {-}

[Meditations of a Porn Addict](https://mega.nz/file/DlxiFACJ#FLglnhxnenKVIDn9JVQHI1v_ZGs1yzC3Kvc0mLf13ds) - Guillaco

[EasyPeasy Statements Checklist](https://pastebin.com/dybv6qkD) - SWATxKATS

[9 Minute Meditation](https://www.youtube.com/watch?v=tw7XBKhZJh4) - Sam Harris

[Free Month of Waking Up Meditation Course](https://share.wakingup.com/a13290) - Sam Harris

[Exiting Modernity](https://meta-nomad.net/exiting-modernity) - Meta Nomad

## REBT Coping Statements {-}

- *“I can stop PMO, even though it appears 'hard' to do so. It’s not too hard, and no matter how much trouble it takes, it’s worth it!”*

- *“If I keep ignoring and never giving into my powerful urges to PMO, I will make it easier and easier to resist them”*

- *“I can fully and unconditionally accept myself -- yes, even with all my flaws and failings”

- *“PMO seems to quickly 'cure' my problems, but actually makes them worse.”*

- *“At times, I'd like very much to drown my troubles in PMO, but that is never a reason to do so.”*

- *“It's most uncomfortable when I don't get what I really want. But it's not awful or terrible unless I choose to believe that it is, and I choose to believe something more realistic and helpful.”*

- *“I’ll never like unfair treatment, but I damned well can stand it and perhaps plot and scheme to stop it.”*

- *“No matter how many times I fail at this important pursuit, my failure never makes me an incompetent louse. It just makes me a person who may have acted incompetently at that time”*

- *“I don't absolutely need what I want, but I can still be reasonably happy, though not as happy as when I don't get it.”*

- *“I strongly prefer to be outstanding at my work, but I don't have to be. Too bad if I'm not, but it doesn't make me inferior. I can always keep trying to do better without needing to do better.”*

- *“Many things can help make me sorry and disappointed, but when I demand and command that these things must not exist, I then make myself panicked, depressed, and enraged.”*

- *“Yes, I've often failed to do what I promised that I'd do, but that doesn't mean that I can't or won't carry out this promise”*

- *“I hate like hell being anxious and depressed, but I don't have to immediately dissolve these feelings with PMO. When PMOing, I temporarily feel better about my problems, but I don't get better. In the long run, PMO makes them worse.”*

- *“People don't enrage me by treating me badly. I pigheadedly choose to enrage myself about their bad treatment by demanding and commanding that they act better.”*
